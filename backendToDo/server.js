const express = require("express");
const mongoose = require("mongoose");
const Router = require("./src/routes")
const cors = require("cors");
const app = express();
const corsOptions = {
  origin: '*',
  credentials: true,            //access-control-allow-credentials:true
  optionSuccessStatus: 200,
}

app.use(cors(corsOptions))
app.use(express.json());


mongoose.connect('mongodb://localhost:27017/todolist');
const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error: "));
db.once("open", function () {
  console.log("Connected successfully");
});

app.use('/', Router);
// app.get('/',(req,res)=>{
//     res.send('Welcome')
// })
app.listen(3000, () => {
  console.log("Server is running at port 3000");
});