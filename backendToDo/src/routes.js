const express = require("express");
const app = express();
const ListRout = require('./List/routes')
const ToDoList = require('./ToDoList/routes')


app.use('/list',ListRout)
app.use('/todolist',ToDoList)

module.exports = app;