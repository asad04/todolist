const express = require("express");
const Controller = require('./controller')
const app = express();

app.get("/", async (req, res) => {
    try {
        Controller.getAllLists(req, res).then((data, err) => {
            if (err) {
                res.status(500).json(err);
            } else if (!data) {
                res.status(404).json();
            }
            console.log(data,'    data   ')
            res.status(200).json(data);
        });
    } catch (error) {
        console.log(error)
        res.send(error.message);
    }
});

app.get("/:id", async (req, res) => {
    console.log(req.params,'   params')
    try {
        Controller.getALists(req, res).then((err, data) => {
            if (err) {
                res.status(500).json(err);
            } else if (!data) {
                res.status(404).json();
            }
            res.status(200).json(data);
        });
    } catch (error) {
        console.log(error)
        res.send(error.message);
    }
});

app.post("/", async (req, res) => {
    try {
        Controller.saveList(req.body, res)
            .then((err, data) => {
                if (err) {
                    res.status(500).json(err);
                } else if (!data) {
                    res.status(404).json();
                }
                res.status(200).json(data);
            });
    } catch (error) {
        console.log(error)
        res.send(error.message);
    }
});

app.put("/:id", async (req, res) => {
    try {
        console.log(req.params)
        Controller.updateList(req.body, res, req.params.id).then((err, data) => {
            if (err) {
                res.status(500).json(err);
            } else if (!data) {
                res.status(404).json();
            }
            res.status(200).json(data);
        });
    } catch (error) {
        console.log(error, '  error')
        res.send(error.message);
    }
});

app.delete("/:id", async (req, res) => {
    try {
        console.log(req.params)
        Controller.deleteList(req.params.id,res).then((err, data) => {
            if (err) {
                res.status(500).json(err);
            } else if (!data) {
                res.status(404).json();
            }
            res.status(200).json(data);
        });
    } catch (error) {
        console.log(error, '  error')
        res.send(error.message);
    }
});

module.exports = app;