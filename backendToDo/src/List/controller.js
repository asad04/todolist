const express = require("express");
const ListModel = require("./model");

const getAllLists = async (req, res) => {
    try {
        let data = await ListModel.find();
        return data;
    } catch (error) {
        res.status(500).send(error);
    }
}

const getALists = async (req, res) => {
    try {
        let data = await ListModel.findById({ _id: req.params.id });
        res.send(data);
    } catch (error) {
        res.status(500).send(error);
    }
}

const saveList = async (req, res) => {
    try {
        let data = new ListModel({ ...req });
        res.send(await data.save());
    } catch (error) {
        res.status(500).send(error);
    }
}

const updateList = async (req, res, id) => {
    try {
        await ListModel.findOneAndUpdate({ _id: id }, { $set: req },
            { new: true, upsert: true, setDefaultsOnInsert: true }
        )
            .then((docs) => res.send(docs))
            .catch((err) => res.status(500).send({ message: err }));
    } catch (err) {
        res.status(500).json({ message: err });
    }
}


const deleteList = async (id, res) => {
    try {
        await ListModel.findByIdAndDelete(
            { _id: id }
        )
            .then((docs) => { res.send('Data deleted Successfully') })
            .catch((err) => res.status(500).send({ message: err }));
    } catch (err) {
        res.status(500).json({ message: err });
    }
}

module.exports = {
    getAllLists,
    saveList,
    updateList,
    deleteList,
    getALists
}