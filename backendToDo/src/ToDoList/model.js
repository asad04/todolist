const mongoose = require("mongoose");

const ToDoListSchema = new mongoose.Schema({
  title: {
    type: String,
  },
  date: {
    type: Date
  },
  listId: { type: mongoose.Schema.Types.ObjectId, ref: 'List' }
}, {
  timestamps: true
});

const ToDoList = mongoose.model("ToDoList", ToDoListSchema);

module.exports = ToDoList;