const express = require("express");
const Controller = require('./controller')
const app = express();


app.get("/", async (req, res) => {
    try {
        Controller.getAllToDoLists(req, res).then((err, company) => {
            if (err) {
                res.status(500).json(err);
            } else if (!company) {
                res.status(404).json();
            }
            res.status(200).json(company);
        });
    } catch (error) {
        console.log(error)
        res.send(error.message);
    }
});

app.get("/:id", async (req, res) => {
    try {
        console.log(req.params)
        Controller.getAToDoLists(req, res).then((data, err) => {
            if (err) {
                res.status(500).json(err);
            } else if (!data) {
                res.status(404).json();
            }
            res.status(200).json(data);
        });
    } catch (error) {
        console.log(error)
        res.send(error.message);
    }
});

app.get("/list:id", async (req, res) => {
    try {
        Controller.getAToDoListsByListID(req.params.id, res).then((err, company) => {
            if (err) {
                res.status(500).json(err);
            } else if (!company) {
                res.status(404).json();
            }
            res.status(200).json(company);
        });
    } catch (error) {
        console.log(error)
        res.send(error.message);
    }
});

app.post("/", async (req, res) => {
    try {
        Controller.saveToDoList(req.body, res)
            .then((err, company) => {
                if (err) {
                    res.status(500).json(err);
                } else if (!company) {
                    res.status(404).json();
                }
                res.status(200).json(company);
            });
    } catch (error) {
        console.log(error)
        res.send(error.message);
    }
});

app.put("/:id", async (req, res) => {
    try {
        console.log(req.params)
        let data = await Controller.updateToDoList(req.body, res, req.params.id);
        console.log(data, '    data ')
    } catch (error) {
        console.log(error, '  error')
        res.send(error.message);
    }
});

app.delete("/:id", async (req, res) => {
    try {
        console.log(req.params)
        let data = await Controller.deleteToDoList(res,req.params.id);
        console.log(data, '    data ')
    } catch (error) {
        console.log(error, '  error')
        res.send(error.message);
    }
});

module.exports = app;