const express = require("express");
const ToDoListModel = require("./model");

const getAllToDoLists = async (req, res) => {
    try {
        let data = await ToDoListModel.find().populate({
            path  : 'listId',
            match : 'List'
          });
        res.send(data);
    } catch (error) {
        res.status(500).send(error);
    }
}

    const getAToDoLists = async (req, res) => {
        try {
            let data = await ToDoListModel.find({ _id: req.params.id }).populate({
                path  : 'listId',
                match : 'List'
              });
            res.send(data);
        } catch (error) {
            res.status(500).send(error);
        }
    }

const getAToDoListsByListID = async (id, res) => {
    try {
        let data = await ToDoListModel.find({listId:id}).populate('List');
        res.send(data);
    } catch (error) {
        res.status(500).send(error);
    }
}

const saveToDoList = async (req, res) => {
    try {
        let data = new ToDoListModel({ ...req });
        res.send(await data.save());
    } catch (error) {
        res.status(500).send(error);
    }
}

const updateToDoList = async (req, res, id) => {
    try {
        await ToDoListModel.findOneAndUpdate({ _id: id }, { $set: req },
            { new: true, upsert: true, setDefaultsOnInsert: true }
        )
            .then((docs) => res.send(docs))
            .catch((err) => res.status(500).send({ message: err }));
    } catch (err) {
        res.status(500).json({ message: err });
    }
}


const deleteToDoList = async (res, id) => {
    try {
        await ToDoListModel.findByIdAndDelete(
            { _id: id }
        )
            .then((docs) => res.send('Data deleted Successfully'))
            .catch((err) => res.status(500).send({ message: err }));
    } catch (err) {
        res.status(500).json({ message: err });
    }
}

module.exports = {
    getAllToDoLists,
    saveToDoList,
    updateToDoList,
    deleteToDoList,
    getAToDoLists,
    getAToDoListsByListID
}