import { tab } from "@testing-library/user-event/dist/tab";
import React from "react";
import EditIcon from '@mui/icons-material/Edit';
import ClearIcon from '@mui/icons-material/Clear';
import { DataTable } from 'primereact/datatable';
import DatePicker from 'react-date-picker'
import { Column } from 'primereact/column';
class TodoList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            date: new Date(),
            list: [],
            listname: '',
            btn: false,
            seletedId: '',
            checkTodo: false,
            selectedRow: null
        }
    }

    componentDidMount() {
        fetch('http://localhost:3000/list')
            .then(response => response.json())
            .then(data => {
                console.log(data, '     data is ')
                this.setState({ list: data })
            }).catch(err => alert(err))

    }

    addList = () => {

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ name: this.state.listname })
        };
        fetch('http://localhost:3000/list', requestOptions)
            .then(response => response.json())
            .then(data => window.location.reload()).catch(err => alert(err))

    }

    onUpdate = () => {
        const requestOptions = {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ name: this.state.listname })
        };
        fetch(`http://localhost:3000/list/${this.state.seletedId}`, requestOptions)
            .then(response => response.json())
            .then(data => window.location.reload()).catch(err => alert(err))
    }

    onChange = (e) => {
        console.log(e, 'its eeeeeeeeeeeeeee')
        this.setState({ date: e })
    }

    onClick = (e) => {
        console.log(e.value)
        this.setState({ checkTodo: true, selectedId: e.value._id })
    };

    onEdit = (data) => {
        this.setState({ listname: data.name, btn: true, seletedId: data._id })
    }

    onDelete = (data) => {
        const requestOptions = {
            method: 'DELETE',
        };
        fetch(`http://localhost:3000/list/${data._id}`, requestOptions)
            .then(response => response)
            .then(data => window.location.reload()).catch(err => { console.log(err); alert(err) })
    }

    editBodyTemplate = (rowData) => {
        return (
            <>
                <i onClick={() => this.onEdit(rowData)} id="editicon" ><EditIcon /></i>
            </>
        );
    };

    deleteBodyTemplate = (rowData) => {
        return (
            <>
                <i onClick={() => this.onDelete(rowData)} id="editicon" ><ClearIcon /></i>
            </>
        );
    };

    render() {
        return <div className="main-container">
            <div className="first-p">
                <div>
                    <input type={'text'} value={this.state.listname} name="listname" onChange={(e) => this.setState({ listname: e.target.value })} placeholder="Enter Name of list" className="textinputlist" ></input>
                    <button onClick={() => { this.state.btn ? this.onUpdate() : this.addList() }} className="addbtn" >{this.state.btn ? 'Update' : 'Add'}</button>
                </div>
                <div>
                    <div className="card">
                        <DataTable value={this.state.list} responsiveLayout="scroll"
                            selectionMode="single" selection={this.state.selectedRow}
                            onSelectionChange={e => this.onClick(e)}
                        >
                            <Column field="name" header="Name"></Column>
                            <Column header="Edit" body={this.editBodyTemplate}> </Column>
                            <Column header="Delete" body={this.deleteBodyTemplate}></Column>

                        </DataTable>
                    </div>
                </div>
            </div>
            <div>
                {this.state.checkTodo && <div>
                    <input type={'text'} placeholder="Enter title" className="textinputlist" ></input>
                    <DatePicker
                        onChange={(e) => this.onChange(e)}
                        value={this.state.date}
                    />
                    <button className="addbtn2" >Add Todo</button>
                </div>}
            </div>
        </div>

    }
}
export default TodoList;